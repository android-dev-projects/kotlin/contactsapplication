package com.example.contactsapplication.repository

import android.app.Application
import android.content.Context.MODE_PRIVATE
import androidx.lifecycle.Transformations
import com.example.contactsapplication.R
import com.example.contactsapplication.model.Contact
import com.example.contactsapplication.model.Options
import com.example.contactsapplication.room.ContactsDatabase
import kotlinx.coroutines.*

class ContactsRepository(private val app: Application) {

    private val database = ContactsDatabase.getInstance(app)

    private var dao = database.contactsDao

    init {
        val sp = app.getSharedPreferences(app.resources.getString(R.string.shared_pref_name), MODE_PRIVATE)

        if(!sp.contains(app.resources.getString(R.string.sort_option_sharedPref))){
            val editor = sp.edit()
            editor.putString(app.resources.getString(R.string.sort_option_sharedPref), "added")
            editor.commit()
            loadSettings()
        }else{
            val opData = sp.getString(app.resources.getString(R.string.sort_option_sharedPref), "NULL")
            if(!opData.equals("added")){
                val editor = sp.edit()
                editor.putString(app.resources.getString(R.string.sort_option_sharedPref), "added")
                editor.commit()
                loadSettings()
            }
        }
    }

    val sortStatus = dao.getOption("sort")

    val ContactsList = Transformations.switchMap(sortStatus) {
        it?.let{
            dao.getAllContacts(it.optValue)
        }
    }

    suspend fun AddContact(newContact: Contact){
        withContext(Dispatchers.IO){
            dao.addContact(newContact)
        }
    }

    suspend fun DeleteAll(){
        withContext(Dispatchers.IO){
            dao.deleteAll()
        }
    }

    suspend fun GetContact(contactId: Int): Contact{
        return dao.getContact(contactId)
    }

    suspend fun DeleteContact(contactId: Int){
        withContext(Dispatchers.IO){
            dao.deleteContact(contactId)
        }
    }

    suspend fun AddOption(option: Options){
        withContext(Dispatchers.IO){
            dao.addOption(option)
        }
    }

    private fun loadSettings(){
        val job = Job()
        val scope = CoroutineScope ( job + Dispatchers.IO )

        scope.launch {
            AddOption(
                    Options(
                            "sort",
                            true
                    )
            )
        }
    }
}