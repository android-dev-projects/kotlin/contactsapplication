package com.example.contactsapplication.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.contactsapplication.R
import com.example.contactsapplication.model.Contact
import com.example.contactsapplication.model.Options


@Database(entities = [Contact::class, Options::class], version = 1)
abstract class ContactsDatabase: RoomDatabase() {
    abstract val contactsDao: ContactsDatabaseDao

    companion object{
        private lateinit var INSTANCE: ContactsDatabase
        fun getInstance(context: Context): ContactsDatabase{
            // equivalent to -> synchronized(ContactsDatabase::class.java)
            synchronized(this){
                if(!::INSTANCE.isInitialized){
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                                                    ContactsDatabase::class.java,
                                                    context.getString(R.string.contacts_database_name))
                                                        .build()
                    return INSTANCE
                }
            }
            return INSTANCE
        }
    }
}