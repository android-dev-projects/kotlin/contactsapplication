package com.example.contactsapplication.room

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.contactsapplication.model.Contact
import com.example.contactsapplication.model.Options

@Dao
interface ContactsDatabaseDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addContact(newContact: Contact)

    @Query("select * from contacts_table order by case when :sort = 1 then name end asc, case when :sort = 0 then name end desc")
    fun getAllContacts(sort: Boolean = true): LiveData<List<Contact>>

    @Query("delete from contacts_table")
    fun deleteAll()

    @Query("select * from contacts_table where id = :contactId")
    fun getContact(contactId: Int): Contact

    @Query("delete from contacts_table where id = :contactId")
    fun deleteContact(contactId: Int)

    @Query("select * from options_table where optName = :name")
    fun getOption(name: String): LiveData<Options>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addOption(opt: Options)
}