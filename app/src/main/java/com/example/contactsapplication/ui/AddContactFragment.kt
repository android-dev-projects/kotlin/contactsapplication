package com.example.contactsapplication.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.contactsapplication.R
import com.example.contactsapplication.databinding.FragmentAddContactBinding
import com.example.contactsapplication.model.Contact
import com.example.contactsapplication.viewmodel.ContactsViewModel
import com.example.contactsapplication.viewmodel.ContactsViewModelFactory
import com.google.android.material.snackbar.Snackbar

class AddContactFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = FragmentAddContactBinding.inflate(layoutInflater)

        val application = requireNotNull(this.activity).application

        val viewModelFactory = ContactsViewModelFactory(application)

        val viewModel = ViewModelProvider(this, viewModelFactory).get(ContactsViewModel::class.java)

        binding.lifecycleOwner = this

        binding.viewModel = viewModel

        viewModel.addNewContact.observe(viewLifecycleOwner, Observer {
            if(binding.contactNameEt.text.toString().trim() == "" || binding.contactNumberEt.text.toString().trim() == ""){
                Snackbar.make(binding.root, getString(R.string.required_fields_warning), Snackbar.LENGTH_LONG).show()
            }
            else{
                it?.let {
                    if(it){
                        viewModel.addContact(Contact(0, binding.contactNameEt.text.toString(), binding.contactNumberEt.text.toString(), 0))
                }
                else if(!it){
                    Snackbar.make(binding.root, getString(R.string.contact_added_successfully), Snackbar.LENGTH_LONG).show()
                    findNavController().navigate(AddContactFragmentDirections.actionAddContactFragmentToContactsListFragment())
                }
            }
            }
        })

        return binding.root
    }
}