package com.example.contactsapplication.ui

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.contactsapplication.R
import com.example.contactsapplication.adapters.ContactsAdapter
import com.example.contactsapplication.utilities.ContactsListener
import com.example.contactsapplication.databinding.FragmentContactsListBinding
import com.example.contactsapplication.viewmodel.ContactsViewModel
import com.example.contactsapplication.viewmodel.ContactsViewModelFactory
import com.google.android.material.snackbar.Snackbar

class ContactsListFragment : Fragment() {

    lateinit var binding: FragmentContactsListBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentContactsListBinding.inflate(layoutInflater)

        val application = requireNotNull(this.activity).application

        val viewModelFactory = ContactsViewModelFactory(application)

        val viewModel = ViewModelProvider(this, viewModelFactory).get(ContactsViewModel::class.java)

        binding.viewModel = viewModel

        binding.lifecycleOwner = this

        binding.contactsRecyclerview.adapter = ContactsAdapter(ContactsListener { contactId ->
            viewModel.initiateOnNavigateToContactDetails(contactId)
        })

        viewModel.navigateToAddContactFragment.observe(viewLifecycleOwner, Observer{
            it?.let{
                if(it){
                    findNavController().navigate(ContactsListFragmentDirections.actionContactsListFragmentToAddContactFragment())
                    viewModel.endNavigateToAddContactFragment()
                }
            }
        })

        viewModel.navigateToContactDetailsFragment.observe(viewLifecycleOwner, Observer{
            it?.let{
                findNavController().navigate(ContactsListFragmentDirections.actionContactsListFragmentToContactDetailsFragment(it))
                viewModel.endOnNavigateToContactDetailsFragment()
            }
        })

        viewModel.deleteAllContactsAction.observe(viewLifecycleOwner, Observer {
            it?.let{
                if(it){
                    if(viewModel.contactsList.value?.size == 0){
                        Snackbar.make(binding.root, getString(R.string.no_contacts_to_delete), Snackbar.LENGTH_LONG).show()
                    }
                    else{
                        viewModel.deleteAll()
                    }
                }
                else{//after delete show success message by observing
                    Snackbar.make(binding.root, getString(R.string.all_contacts_deleted_successfulyy), Snackbar.LENGTH_LONG).show()
                }
            }
        })

        viewModel.sortAZ.observe(viewLifecycleOwner, Observer {
            it?.let{
                if(it){
                    viewModel.toggleAZ()
                }
            }
        })

        viewModel.sortZA.observe(viewLifecycleOwner, Observer {
            it?.let{
                if(it){
                    viewModel.toggleZA()
                }
            }
        })

        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.list_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            R.id.menu_delete_all -> {
                binding.viewModel?.initiateDeleteAllContacts()
                true
            }
            R.id.menu_sort_a_z ->  {
                binding.viewModel?.initiateAZ()
                true
            }
            R.id.menu_sort_z_a -> {
                binding.viewModel?.initiateZA()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
