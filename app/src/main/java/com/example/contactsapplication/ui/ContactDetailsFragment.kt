package com.example.contactsapplication.ui

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.contactsapplication.R
import com.example.contactsapplication.databinding.FragmentContactDetailsBinding
import com.example.contactsapplication.viewmodel.ContactsViewModel
import com.example.contactsapplication.viewmodel.ContactsViewModelFactory
import com.google.android.material.snackbar.Snackbar

class ContactDetailsFragment : Fragment() {
   override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
       val binding = FragmentContactDetailsBinding.inflate(layoutInflater)

       var arguments = ContactDetailsFragmentArgs.fromBundle(requireArguments())

       val application = requireNotNull(activity).application

       val viewModelFactory = ContactsViewModelFactory(application)

       val viewModel = ViewModelProvider(this, viewModelFactory).get(ContactsViewModel::class.java)

       binding.lifecycleOwner = this

       binding.viewModel = viewModel

       viewModel.getContact(arguments.contactId)

      viewModel.contact.observe(viewLifecycleOwner, Observer{
          it?.let {
              binding.contact = it
          }
       })

       viewModel.deleteContactAction.observe(viewLifecycleOwner, Observer {
           it?.let {
               if(it){
                  viewModel.deleteContact(viewModel.contact.value!!.id)
               }
               else{
                   Snackbar.make(binding.root, getString(R.string.contact_deleted_successfully), Snackbar.LENGTH_LONG).show()
                   findNavController().navigate(ContactDetailsFragmentDirections.actionContactDetailsFragmentToContactsListFragment())
               }
           }
       })

       viewModel.toggleSaveButton.observe(viewLifecycleOwner, Observer{
           it?.let{
               binding.viewState = it
               if(!it)
                   viewModel.initiateEditContact()
           }
       })

       viewModel.editContact.observe(viewLifecycleOwner, Observer{
           it?.let{
               if(it){
                   viewModel.addContact(binding.contact!!)
                   viewModel.endEditContact()
                   viewModel.getContact(binding.contact!!.id)
               }
           }
       })

        return binding.root
    }
}