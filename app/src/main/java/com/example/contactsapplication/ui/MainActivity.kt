package com.example.contactsapplication.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.contactsapplication.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initSettingsData()

    }

    private fun initSettingsData() {
        val sharedPreferences = getSharedPreferences(getString(R.string.shared_pref_name), MODE_PRIVATE)

        if (!sharedPreferences.contains(getString(R.string.sort_option_sharedPref))) {
            val editor = sharedPreferences.edit()

            editor.putString(getString(R.string.sort_option_sharedPref), "none")

            editor.commit()
        }
    }
}