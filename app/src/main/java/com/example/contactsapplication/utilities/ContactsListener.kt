package com.example.contactsapplication.utilities

class ContactsListener(val clickListener: (contactId: Int) -> Unit){
    fun onClick(contactId: Int) = clickListener(contactId)
}