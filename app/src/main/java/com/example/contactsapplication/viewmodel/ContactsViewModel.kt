package com.example.contactsapplication.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.contactsapplication.model.Contact
import com.example.contactsapplication.model.Options
import com.example.contactsapplication.repository.ContactsRepository
import kotlinx.coroutines.*

class ContactsViewModel(val app: Application): AndroidViewModel(app) {

    private val viewModelJob = Job()

    private val viewModelScope = CoroutineScope(viewModelJob + Dispatchers.IO)

    private val repo = ContactsRepository(app)

    private val _navigateToAddContactFragment = MutableLiveData<Boolean>()

    val navigateToAddContactFragment: LiveData<Boolean>
        get() = _navigateToAddContactFragment

    private val _navigateToContactDetailsFragment = MutableLiveData<Int>()

    val navigateToContactDetailsFragment: LiveData<Int>
        get() = _navigateToContactDetailsFragment

    private val _addNewContact = MutableLiveData<Boolean>()

    val addNewContact: LiveData<Boolean>
        get() = _addNewContact

    private val _deleteAllContactsAction = MutableLiveData<Boolean>()

    val deleteAllContactsAction: LiveData<Boolean>
        get() = _deleteAllContactsAction

    private val _deleteContactAction = MutableLiveData<Boolean>()

    val deleteContactAction: LiveData<Boolean>
        get() = _deleteContactAction

    private val _toggleSaveButton = MutableLiveData<Boolean>()

    val toggleSaveButton: LiveData<Boolean>
        get() = _toggleSaveButton

    private val _editContact = MutableLiveData<Boolean>()

    val editContact: LiveData<Boolean>
        get() = _editContact

    private val _contact = MutableLiveData<Contact>()

    val contact: LiveData<Contact>
        get() = _contact

    private val _sortAZ = MutableLiveData<Boolean>()

    val sortAZ: LiveData<Boolean>
            get() = _sortAZ

    private val _sortZA = MutableLiveData<Boolean>()

    val sortZA: LiveData<Boolean>
            get() = _sortZA

    val sortStatus = repo.sortStatus

    val contactsList = repo.ContactsList

    fun addContact(newContact: Contact){//check for status to return from frag
        viewModelScope.launch {
            repo.AddContact(newContact)
        }
        endAddNewContact()
    }

    fun deleteAll(){
        viewModelScope.launch {
            repo.DeleteAll()
        }
        endDeleteAllContacts()
    }

    fun getContact(contactId: Int){
       viewModelScope.launch {
           withContext(Dispatchers.IO){
               _contact.postValue(repo.GetContact(contactId))
           }
       }
    }

    fun deleteContact(contactId: Int){
        viewModelScope.launch {
            repo.DeleteContact(contactId)
        }
        endDeleteContact()
    }

    fun initiateOnNavigateToAddContactFragment(){
        _navigateToAddContactFragment.value = true
    }

    fun endNavigateToAddContactFragment(){
        _navigateToAddContactFragment.value = false
    }

    fun initiateOnNavigateToContactDetails(contactId: Int){
        _navigateToContactDetailsFragment.value = contactId
    }

    fun endOnNavigateToContactDetailsFragment(){
        _navigateToContactDetailsFragment.value = null
    }

    fun initiateAddNewContact(){
        _addNewContact.value = true
    }

    fun endAddNewContact(){
        _addNewContact.value = false
    }

    fun initiateDeleteAllContacts(){
        _deleteAllContactsAction.value = true
    }
    fun endDeleteAllContacts(){
        _deleteAllContactsAction.value = false
    }

    fun initiateDeleteContact(){
        _deleteContactAction.value = true
    }
    fun endDeleteContact(){
        _deleteContactAction.value = false
    }

    fun initiateSaveButton(){
        _toggleSaveButton.value = true
    }
    fun endSaveButton(){
        _toggleSaveButton.value = false
    }

    fun initiateEditContact(){
        _editContact.value = true
    }

    fun endEditContact(){
        _editContact.value = false
    }

    fun toggleAZ(){
        toggleSort(Options("sort", true))
        terminateAZ()
    }

    fun toggleZA(){
        toggleSort(Options("sort", false))
        terminateZA()
    }

    fun toggleSort(opt: Options){
        viewModelScope.launch {
            repo.AddOption(opt)
        }
    }

    fun initiateAZ(){
        _sortAZ.value = true
    }

    fun terminateAZ(){
        _sortAZ.value = false
    }

    fun initiateZA(){
        _sortZA.value = true
    }

    fun terminateZA(){
        _sortZA.value = false
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}