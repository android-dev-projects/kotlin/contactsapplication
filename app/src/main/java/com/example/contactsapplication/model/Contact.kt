package com.example.contactsapplication.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "contacts_table")
data class Contact(
    @PrimaryKey(autoGenerate = true)
    var id: Int,
    var name: String,
    var number: String,
    var image: Int
)
