package com.example.contactsapplication.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "options_table")
data class Options(
        @PrimaryKey(autoGenerate = false)
        val optName: String,
        val optValue: Boolean
)
