package com.example.contactsapplication.adapters

import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.contactsapplication.model.Contact

@BindingAdapter("contact_image")
fun ImageView.setImage(imageId: Int){
    this.setImageResource(imageId)
}

@BindingAdapter("contact_name")
fun TextView.setName(name: String){
    this.text = name
}

@BindingAdapter("contact_number")
fun TextView.setNumber(number: String){
    this.text = number
}

@BindingAdapter("listData")
fun RecyclerView.listData(data: List<Contact>?){
    val adapter = this.adapter as ContactsAdapter
    data?.let {
        adapter.submitList(data)
    }
}