package com.example.contactsapplication.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.contactsapplication.databinding.ContactListItemBinding
import com.example.contactsapplication.model.Contact
import com.example.contactsapplication.utilities.ContactsListener

class ContactsAdapter(private val contactListener: ContactsListener): ListAdapter<Contact, ContactsAdapter.ContactsViewHolder>(ContactDiffUtil()) {

    class ContactsViewHolder(private val binding: ContactListItemBinding): RecyclerView.ViewHolder(binding.root){

        companion object{
            fun from(parent: ViewGroup): ContactsViewHolder{
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ContactListItemBinding.inflate(layoutInflater, parent, false)
                return ContactsViewHolder(binding)
            }
        }

        fun bind(contact: Contact, contactListener: ContactsListener){
            binding.contact = contact
            binding.clickListener = contactListener
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactsViewHolder {
        return ContactsViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ContactsViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, contactListener)
    }
}

